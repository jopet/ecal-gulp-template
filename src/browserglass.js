var express = require('express');
var exphbs = require('express-handlebars');
var args = require('yargs').argv;
var app = express();
var fs = require('fs');
// app.engine('.hbs', exphbs({
// 	layoutsDir: 'src/views/layouts',
// 	defaultLayout:'main',
// 	extname: '.hbs',
// }));
// app.set('view engine', '.hbs');
// app.set('views', 'src/views/');
app.set('port', process.env.PORT || 8007);
app.use(express.static('./dist'));

app.get('/', function(req, res) {
	res.writeHead(200, { 'Content-Type': 'text/html' });
	res.end(fs.readFileSync('./dist/index.html'));
});
// app.get('/', function(req, res) {
// 	res.render('index');
// });
// 404 catch-all handler (middleware)
// app.use(function(req, res, next){
// 	res.status(404);
// 	res.render('404');
// });

// // 500 error handler (middleware)
// app.use(function(err, req, res, next){
// 	console.error(err.stack);
// 	res.status(500);
// 	res.render('500');
// });

app.listen(app.get('port'), function () {
	console.log('Express server listening on port ' + app.get('port'));
	console.log('env = ' + app.get('env') +
		'\n__dirname = ' + __dirname +
		'\nprocess.cwd = ' + process.cwd());
});

